import 'package:flutter/material.dart';
import 'package:coffee_shop/views/pages/homePage.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height =MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: width,
          child: Column(
              children: <Widget>[
                SizedBox(height: height * 0.08,),
                Image.asset(
                  "assets/login.png",
                  width: width * 0.5,
                ),
                SizedBox(height: 10,),
                Text(
                  "MAKINI COFFEE SUKAPURA",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 26,
                      color: Color.fromRGBO(27,27,27,1)),
                ),
                SizedBox(height: 5,),
                Text("THE BEST CHOICE",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: Colors.grey
                ),),
                SizedBox(height: 25,),
                Container(
                  margin: EdgeInsets.only(right: 20, left: 20),
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(top: 20, bottom: 20,),
                      prefixIcon: Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20,),
                        child: Icon(Icons.person_outline),
                      ),
                      filled: true,
                      fillColor: Colors.white.withOpacity(0.7),
                      hintText: "Email",
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide: BorderSide(
                          color: Colors.blue,
                          width: 2
                        )
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide: BorderSide(
                          width: 0,
                          style: BorderStyle.none
                        ),
                      )
                    ),
                  ),
                ),
                SizedBox(height: 15,),
                Container(
                  margin: EdgeInsets.only(right: 20, left: 20),
                  child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(top: 20, bottom: 20,),
                        prefixIcon: Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20,),
                          child: Icon(Icons.remove_red_eye),
                        ),
                        filled: true,
                        fillColor: Colors.white.withOpacity(0.7),
                        hintText: "Password",
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: BorderSide(
                                color: Colors.blue,
                                width: 2
                            )
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none
                          ),
                        )
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 10, 30, 0),
                      child: Text(
                        "Forgot Password?",
                        style: TextStyle(
                          color: Color.fromRGBO(27, 27, 27, 0.5),
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                        ),
                      ),
                ],
            ),
          SizedBox(height: 25,),
          RaisedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomePage()),
              );
            },
            highlightElevation: 0,
            focusElevation: 0,
            splashColor: Colors.white.withOpacity(0.1),
            padding: EdgeInsets.fromLTRB(60, 17, 60, 16),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            color: Color.fromRGBO(1, 72, 164, 1),
            child: Text("LOGIN", style: TextStyle(
              fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white
            ),),
          ),
        ],
      ),
      ),
    ),
    );
  }
}