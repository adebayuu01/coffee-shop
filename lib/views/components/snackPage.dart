import 'package:coffee_shop/model/snackModel.dart';
import 'package:flutter/material.dart';

import 'menuSnack.dart';

class SnackPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: menusnack.length,
        itemBuilder: (context, int key) {
          return MenuSnack(index: key);
        },
      ),
    );
  }
}
