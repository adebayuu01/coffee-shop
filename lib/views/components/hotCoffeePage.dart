import 'package:coffee_shop/model/hotCoffeeModel.dart';
import 'package:flutter/material.dart';

import 'menuHotCoffe.dart';

class HotCoffeePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: menu.length,
        itemBuilder: (context, int key) {
          return MenuHotCoffee(index: key);
        },
      ),
    );
  }
}
