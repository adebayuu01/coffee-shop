import 'package:coffee_shop/model/coldCoffeeModel.dart';
import 'package:flutter/material.dart';

import 'menuColdCoffee.dart';

class ColdCoffeePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: menucold.length,
        itemBuilder: (context, int key) {
          return MenuColdCoffee(index: key);
        },
      ),
    );
  }
}
