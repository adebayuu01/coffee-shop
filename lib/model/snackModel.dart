class Coffee {
  final String name;
  final String type;
  final String shortDesc;
  final String desc;
  final String image;
  final int rate;
  final int price;

  Coffee(
      {required this.name,
        required this.type,
        required this.shortDesc,
        required this.desc,
        required this.image,
        required this.rate,
        required this.price});
}

List<Coffee> menusnack = [
  Coffee(
      name: "Ayam Geprek",
      type: 'Makanan Berat',
      shortDesc: "Ayam Geprek dengan sambal yang nikmat.",
      desc:
      "Ayam Geprek khas Makini yang disajikan dengan sambal yang nikmat dan khas dari daerah Sukapura, Kabupaten Probolinggo, Jawa Timur.",
      image: "assets/geprek.jpeg",
      rate: 5,
      price: 13000),
  Coffee(
      name: "Kentang Goreng",
      type: "Snack",
      shortDesc: "Snack kentang goreng yang dipilih dari kentang pilihan.",
      desc:
      "Snack kentang goreng yang dipilih dari kentang pilihan dan disajikan dengan tekstur yang krispy diluar dan lembut didalam.",
      image: "assets/kentang.jpg",
      rate: 5,
      price: 10000),
  Coffee(
      name: "Indomie",
      type: "Snack",
      shortDesc: "Indomie seleraku",
      desc:
      "Indomie goreng yang memiliki cita rasa nusantara yang disajikan dengan telor mata sapi.",
      image: "assets/indomi.jpg",
      rate: 5,
      price: 12000),
];