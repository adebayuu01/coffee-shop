class Coffee {
  final String name;
  final String type;
  final String shortDesc;
  final String desc;
  final String image;
  final int rate;
  final int price;

  Coffee(
      {required this.name,
        required this.type,
        required this.shortDesc,
        required this.desc,
        required this.image,
        required this.rate,
        required this.price});
}

List<Coffee> menucold = [
  Coffee(
      name: "Ice Cappuccino",
      type: 'Ice',
      shortDesc: "Esspresso dengan rasa lebih mild dan disajikan dingin",
      desc:
      "Cappuccino adalah keseimbangan sempurna dariespresso, susu kukus, dan busa.Kopi ini adalah tentang strukturnya dan pemisahan acara dari semua elemen menjadi sepertiga yang sama. Dibuat secara ahli cappuccino harus kaya, tapi tidak asam dan memiliki rasa yang agak manis dari susu. Dan, karena susu tidak benar-benar dicampur di dalamnya memberikan rasa espresso yang lebih kuat.",
      image: "assets/capp.jpg",
      rate: 5,
      price: 15000),
  Coffee(
      name: "Ice Macchiato",
      type: "Ice",
      shortDesc: "Rasa Macchiato yang nikmat dan disajikan dingin.",
      desc:
      "Macchiato kadang disebut espresso macchiato, adalah minuman kopi espresso dengan sedikit susu, biasanya berbusa. Dalam bahasa Italia, macchiato artinya ""bernoda"" atau ""berbintik"", jadi literalnya terjemahan dari caffè macchiato adalah ""kopi bernoda"" atau ""kopi bertanda"".",
      image: "assets/maccic.jpg",
      rate: 5,
      price: 23000),
  Coffee(
      name: " Ice Espresso",
      type: "Ice",
      shortDesc: "Ekstrak kopi dengan rasa kopi yang kuat dan disajikan dingin",
      desc:
      "Minuman kopi paling dasar ini biasanya disajikan dalam demitasse alias cangkir khusus espresso berukuran 30 mililiter (satu shot) sampai 118 mililiter. Espresso bertekstur pekat dan pahit, dengan buih putih alias crema di atasnya yang terbentuk dari tekanan minyak dalam bijih kopi.",
      image: "assets/es.jpg",
      rate: 5,
      price: 12000),
  Coffee(
      name: "Ice Tea",
      type: "Ice",
      shortDesc: "Teh yang memiliki rasa khas dan disajikan dingin",
      desc:
      "Teh adalah minuman aromatik yang disiapkan oleh menuangkan air panas atau mendidih ke atas yang sudah sembuh atau daun segar Camellia sinensis, semak cemara asli Cina, India dan negara-negara Asia Timur lainnya. Teh juga jarang dibuat dari daunnya dari Camellia taliensis..",
      image: "assets/tea.jpg",
      rate: 5,
      price: 10000),
];